<svg width="24.000000" height="24.000000" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
	<desc>
			Created with Pixso.
	</desc>
	<defs>
		<clipPath id="clip189_6939">
			<rect id="clock" width="24.000000" height="24.000000" fill="white" fill-opacity="0"/>
		</clipPath>
	</defs>
	<rect id="clock" width="24.000000" height="24.000000" fill="#FFFFFF" fill-opacity="0"/>
	<g clip-path="url(#clip189_6939)">
		<path id="Vector" d="M12 21C7.02 21 3 16.97 3 12C3 7.02 7.02 3 12 3C16.97 3 21 7.02 21 12C21 16.97 16.97 21 12 21Z" stroke="#3C414B" stroke-opacity="1.000000" stroke-width="1.200000" stroke-linejoin="round"/>
		<path id="Vector" d="M12 7L12 12L15 15" stroke="#3C414B" stroke-opacity="1.000000" stroke-width="1.200000" stroke-linejoin="round" stroke-linecap="round"/>
	</g>
</svg>
