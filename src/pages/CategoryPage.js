import {Fragment, useEffect, useState} from 'react'
import { Menu, Transition } from '@headlessui/react'
import { ChevronDownIcon, FunnelIcon, Squares2X2Icon, ListBulletIcon } from '@heroicons/react/20/solid'
import { useDispatch, useSelector } from 'react-redux';
import { getProductsByCategoryIdAction } from "../store/products/actions";
import {getCurrentCategoryAction} from "../store/categories/actions";
import ProductItemGrid from "../components/productItem/ProductItemGrid";
import ProductItemList from "../components/productItem/ProductItemList";
import FilterMobile from "../components/core/FilterMobile";
import Filter from "../components/core/Filter";
import categoriesFixture from "../store/categories/fixture";
import sortFixture from "../store/fixtures/sort";
import filtersFixture from "../store/fixtures/filters";

const sortOptions = sortFixture;
const subCategories = categoriesFixture;
const filters = filtersFixture;

function classNames(...classes) {
	return classes.filter(Boolean).join(' ');
}

export const CategoryPage = () => {
	const dispatch = useDispatch();
	const products = useSelector(state => state.products.products);
	const category = useSelector(state => state.categories.current_category);
	const [mobileFiltersOpen, setMobileFiltersOpen] = useState(true);
	const [typeProductView, setTypeProductView] = useState('list');

	const handleMobileFiltersOpen = (newValue) => {
		setMobileFiltersOpen(newValue);
	};

	let loadingPage = false;

	useEffect(() => {
		if (!loadingPage) {
			// eslint-disable-next-line react-hooks/exhaustive-deps
			loadingPage = true;

			if (!category.length) {
				dispatch(getCurrentCategoryAction());
			}
			if (!products.length) {
				dispatch(getProductsByCategoryIdAction(category.id));
			}
		}
	}, []);

	return (
		<div className="bg-white">
			<div>
				<FilterMobile subCategories={subCategories} filters={filters} mobileFiltersOpen={mobileFiltersOpen} onChange={handleMobileFiltersOpen}/>

				<main className="mx-auto max-w-7xl px-4 sm:px-6 lg:px-8">
					<div className="flex items-baseline justify-between border-b border-gray-200 pb-6 pt-24">
						<h1 className="text-4xl font-bold tracking-tight text-gray-900">New Arrivals</h1>

						<div className="flex items-center">
							<Menu as="div" className="relative inline-block text-left">
								<div>
									<Menu.Button className="group inline-flex justify-center text-sm font-medium text-gray-700 hover:text-gray-900">
										Sort
										<ChevronDownIcon
											className="-mr-1 ml-1 h-5 w-5 flex-shrink-0 text-gray-400 group-hover:text-gray-500"
											aria-hidden="true"
										/>
									</Menu.Button>
								</div>

								<Transition
									as={Fragment}
									enter="transition ease-out duration-100"
									enterFrom="transform opacity-0 scale-95"
									enterTo="transform opacity-100 scale-100"
									leave="transition ease-in duration-75"
									leaveFrom="transform opacity-100 scale-100"
									leaveTo="transform opacity-0 scale-95"
								>
									<Menu.Items className="absolute right-0 z-10 mt-2 w-40 origin-top-right rounded-md bg-white shadow-2xl ring-1 ring-black ring-opacity-5 focus:outline-none">
										<div className="py-1">
											{sortOptions.map((option) => (
												<Menu.Item key={option.name}>
													{({ active }) => (
														<a
															href={option.href}
															className={classNames(
																option.current ? 'font-medium text-gray-900' : 'text-gray-500',
																active ? 'bg-gray-100' : '',
																'block px-4 py-2 text-sm'
															)}
														>
															{option.name}
														</a>
													)}
												</Menu.Item>
											))}
										</div>
									</Menu.Items>
								</Transition>
							</Menu>

							{typeProductView === 'list' ? (
								<button type="button"
										className="-m-2 ml-5 p-2 text-gray-400 hover:text-gray-500 sm:ml-7"
										onClick={() => setTypeProductView('grid')}>
									<span className="sr-only">View list</span>
									<ListBulletIcon className="h-5 w-5" aria-hidden="true" />
								</button>
							) : (
								<button type="button"
										className="-m-2 ml-5 p-2 text-gray-400 hover:text-gray-500 sm:ml-7"
										onClick={() => setTypeProductView('list')}>
									<span className="sr-only">View grid</span>
									<Squares2X2Icon className="h-5 w-5" aria-hidden="true" />
								</button>
							)}

							<button
								type="button"
								className="-m-2 ml-4 p-2 text-gray-400 hover:text-gray-500 sm:ml-6 lg:hidden"
								onClick={() => setMobileFiltersOpen(true)}
							>
								<span className="sr-only">Filters</span>
								<FunnelIcon className="h-5 w-5" aria-hidden="true" />
							</button>
						</div>
					</div>

					<section aria-labelledby="products-heading" className="pb-24 pt-6">
						<h2 id="products-heading" className="sr-only">
							Products
						</h2>

						<div className="grid grid-cols-1 gap-x-8 gap-y-10 lg:grid-cols-4">
							<Filter subCategories={subCategories} filters={filters} />

							<div className="lg:col-span-3">
								{ typeProductView === 'list' ? (
									<div className="-mx-px grid grid-cols-1 border-l border-gray-200 sm:mx-0">
										{products.map((product) => (
											<ProductItemList product={product} />
										))}
									</div>
								) : (
									<div className="-mx-px grid grid-cols-2 border-l border-gray-200 sm:mx-0 md:grid-cols-3 lg:grid-cols-4">
										{products.map((product) => (
											<ProductItemGrid product={product} />
										))}
									</div>
								)}
							</div>
						</div>
					</section>
				</main>
			</div>
		</div>
	)
}
