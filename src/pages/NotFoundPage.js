import React from 'react';

export const NotFoundPage = () => {
	return (
		<div className="container my-14 m-auto">
			<h1 key="title" className="text-3xl font-bold underline">NotFoundPage</h1>
			<p>description</p>
		</div>
	)
}
