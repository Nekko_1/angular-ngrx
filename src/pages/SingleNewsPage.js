import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getCurrentPostAction } from '../store/posts/actions';
export const post = (state) => state.posts.posts;

export const SingleNewsPage = () => {
    const post = useSelector(state => state.posts.current_post);
    const dispatch = useDispatch();

    let loadingPage = false;

    useEffect(() => {
        if (!loadingPage) {
            // eslint-disable-next-line react-hooks/exhaustive-deps
            loadingPage = true;

            if (!post.length) {
                dispatch(getCurrentPostAction());
            }
        }
    }, []);

    return (
        <div className="container my-14 m-auto">
            {
                post.id && (
                    <>
                        <div className="featured-post__background mb-5"
                             style={{backgroundImage: 'url(' + post.featured_image + ')'}}>
                        </div>
                        <div className="post-box__text-content">
                            <h3 className="font-semibold mb-3">{post.name}</h3>
                            <p className="mb-3">{post.description}</p>
                            <p className="text-zinc-500">{post.published_at}</p>
                        </div>
                    </>
                )
            }
        </div>
	)
};
