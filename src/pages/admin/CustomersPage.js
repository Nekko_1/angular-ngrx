import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {logoutAction} from "../../store/customers/actions";

export const CustomersPage = () => {
	const dispatch = useDispatch();
	const currentCustomer = useSelector(state => state.customers.current_customer);

	return (
		<div className="container my-14 m-auto">
			<h1 key="title" className="text-3xl font-bold underline">Dashboard</h1>
			<pre>{currentCustomer.name}</pre>
			<pre>{currentCustomer.email}</pre>
			<a href="/" onClick={() => { dispatch(logoutAction()) }}>Logout</a>
		</div>
	)
}
