import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getPostsAction, setCurrentPostAction } from '../../store/posts/actions';
import { Link } from 'react-router-dom';

export const ProductsPage = () => {
	const dispatch = useDispatch();
	const posts = useSelector(state => state.posts.posts);

	let loadingPage = false;

	useEffect(() => {
		if (!loadingPage) {
			// eslint-disable-next-line react-hooks/exhaustive-deps
			loadingPage = true;

			if (!posts.length) {
				dispatch(getPostsAction());
			}
		}
	}, []);

    const truncate = (str) => {
        return str.length > 60 ? str.substring(0, 60) + "..." : str;
    };
	
	return (
		<div className="container my-14 m-auto">
            <h2 className="text-3xl font-bold">All Posts</h2>

			<div className="flex flex-row flex-wrap">
				{
					posts.length > 0 && (
						<>
							{

								posts.map(post =>
									<div className="basis-1/4 pr-4 my-10">
										<Link to={'/post/' + post.url_key} className="post-box" onClick={() => dispatch(setCurrentPostAction(post))}>
											<div className="featured-post__background mb-5" style={{ backgroundImage: 'url(' + post.featured_image + ')' }}></div>
											<div className="post-box__text-content">
												<h3 className="font-semibold mb-3">{post.name}</h3>
												<p className="mb-3">{truncate(post.description)}</p>
												<p className="text-zinc-500">{post.published_at}</p>
											</div>
										</Link>
									</div>
								)
							}
						</>
					)
				}
			</div>
		</div>
	)
}
