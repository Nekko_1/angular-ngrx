import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import LatestPosts from '../components/latestPosts/LatestPosts';
import {getProductsAction} from "../store/products/actions";
import {getLatestPostsAction, getPostsAction} from "../store/posts/actions";

export const HomePage = () => {
	const dispatch = useDispatch();
	const products = useSelector(state => state.products.products);
	const posts = useSelector(state => state.posts.posts);
	const latestPosts = useSelector(state => state.posts.latest_posts);

	let loadingPage = false;

	useEffect(() => {
		if (!loadingPage) {
			// eslint-disable-next-line react-hooks/exhaustive-deps
			loadingPage = true;

			if (!products.length) {
				dispatch(getProductsAction());
			}
			if (!posts.length) {
				dispatch(getPostsAction());
			}
			if (!latestPosts.length) {
				dispatch(getLatestPostsAction());
			}
		}
	}, []);

	return (
		<div className="container my-14 m-auto">
			<h1 key="title" className="text-3xl font-bold underline">
				Homepage
			</h1>

			{
				products.length > 0 && (
					products.map(product =>
						<div className="product" key={'product-' + product.id}>
							<h3 className="text-black py-2 px-4">{product.title}</h3>
						</div>
					)
				)
			}

			<LatestPosts posts={latestPosts}></LatestPosts>
		</div>
	)
}
