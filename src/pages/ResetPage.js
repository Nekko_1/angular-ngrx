import React from 'react';

export const ResetPage = () => {
	return (
		<div className="container my-14 m-auto">
			<h1 key="title"  className="text-3xl font-bold underline">Password Reset</h1>
		</div>
	)
}
