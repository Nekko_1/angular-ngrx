export enum POSTS_TYPES {
    GET_POSTS = "GET_POSTS",
    GET_POST_BY_ID = "GET_POST_BY_ID",
    SET_MANY_POSTS = "SET_MANY_POSTS",
    GET_LATEST_POSTS = "GET_LATEST_POSTS",
    SET_LATEST_POSTS = "SET_LATEST_POSTS",
    GET_CURRENT_POST = "GET_CURRENT_POST",
    SET_CURRENT_POST = "SET_CURRENT_POST",
}

export interface IPost {
    id: number,
    name: string,
    description: string,
    published_at: string,
    featured_image: string,
    url_key: string
}

export interface PostState {
    posts: IPost[];
    current_post: IPost;
    latest_posts: IPost[];
}

export const initialState: PostState = {
    posts: [],
    current_post: {
        id: 0,
        name: "",
        description: "",
        published_at: "",
        featured_image: "",
        url_key: ""
    },
    latest_posts: []
}

interface getPostsAction {
    type: POSTS_TYPES.GET_POSTS;
    posts: IPost;
}

interface getPostByIdAction {
    type: POSTS_TYPES.GET_POST_BY_ID;
    post: IPost;
}

interface setManyPostsAction {
    type: POSTS_TYPES.SET_MANY_POSTS;
    posts: IPost[];
}

interface setLatestPostsAction {
    type: POSTS_TYPES.SET_LATEST_POSTS;
    posts: IPost[];
}

interface getLatestPostsAction {
    type: POSTS_TYPES.GET_LATEST_POSTS;
    posts: IPost[];
}

interface getCurrentPostsAction {
    type: POSTS_TYPES.GET_CURRENT_POST;
    post: IPost;
}

interface setCurrentPostsAction {
    type: POSTS_TYPES.SET_CURRENT_POST;
    post: IPost;
}

export type PostsAction =  getPostsAction | getPostByIdAction | setManyPostsAction | setLatestPostsAction | getLatestPostsAction | getCurrentPostsAction | setCurrentPostsAction