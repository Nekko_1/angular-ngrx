import {POSTS_TYPES, IPost} from "./post";

export const getPostsAction =() => ({
   type: POSTS_TYPES.GET_POSTS
});

export const getPostsByIdAction = (id: number) => ({
    type: POSTS_TYPES.GET_POST_BY_ID,
    id
});

export const setManyPostsAction = (posts: IPost[]) => ({
    type: POSTS_TYPES.SET_MANY_POSTS,
    posts
});

export const getLatestPostsAction = () => ({
    type: POSTS_TYPES.GET_LATEST_POSTS
});

export const setLatestPostsAction = (posts: IPost[]) => ({
    type: POSTS_TYPES.SET_LATEST_POSTS,
    posts
});

export const getCurrentPostAction = () => ({
    type: POSTS_TYPES.GET_CURRENT_POST
});

export const setCurrentPostAction = (post: IPost) => ({
    type: POSTS_TYPES.SET_CURRENT_POST,
    post
});