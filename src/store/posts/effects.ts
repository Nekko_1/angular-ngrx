import {put, select, takeEvery} from "redux-saga/effects";
import {setCurrentPostAction, setLatestPostsAction, setManyPostsAction} from "./actions";
import {IPost, POSTS_TYPES} from "./post";

function* getPosts() {
    try {
        yield put(setManyPostsAction([{
            id: 1,
            name: "Кошки",
            description: "Кошки - любимые домашние питомцы с уникальными характерами и поведением. В Африке живут дикие кошки, такие как львы, тигры и гепарды, которые поражают своей силой и грацией.",
            published_at: "31.01.2024",
            featured_image: 'https://gas-kvas.com/grafic/uploads/posts/2023-10/1696581493_gas-kvas-com-p-kartinki-vsyakie-10.jpg',
            url_key: 'cats'
        },{
            id: 2,
            name: "Обезьяны",
            description: "Обезьяны - удивительные животные, обитающие в Африке и Азии. Они известны своей ловкостью, любознательностью и забавными выходками.",
            published_at: "31.01.2024",
            featured_image: 'https://gas-kvas.com/grafic/uploads/posts/2023-10/1696581503_gas-kvas-com-p-kartinki-vsyakie-48.jpg',
            url_key: 'monkeys'
        },{
            id: 3,
            name: "Сурикаты",
            description: "Сурикаты - маленькие пушистые друзья с большими головами и длинными хвостами. Они живут в африканских пустынях и известны своим дружелюбием и бесстрашием. ",
            published_at: "31.01.2024",
            featured_image: 'https://gas-kvas.com/grafic/uploads/posts/2023-10/1696581491_gas-kvas-com-p-kartinki-vsyakie-5.jpg',
            url_key: 'suricats'
        },{
            id: 4,
            name: "Африканские перевозки",
            description: "Африканские перевозки - это сложный и важный сектор, включающий разнообразные транспортные системы и логистические решения для развития экономики и социальной сферы африканских стран.",
            published_at: "31.01.2024",
            featured_image: 'https://gas-kvas.com/grafic/uploads/posts/2023-10/thumbs/1696581503_gas-kvas-com-p-kartinki-vsyakie-50.jpg',
            url_key: 'african-transport'
        },{
            id: 5,
            name: "Ежи",
            description: "Ежи - забавные и осторожные зверьки с иголками на спине. Они обитают в Африке и Европе, питаются насекомыми и фруктами.",
            published_at: "31.01.2024",
            featured_image: 'https://www.realtor.com/wp-content/uploads/2015/06/hedgehog.jpg',
            url_key: 'hedgehogs'
        },{
            id: 6,
            name: "Вулканы",
            description: "Вулканы - это горы с активным вулканизмом, которые могут извергаться и разрушаться. В Африке есть множество вулканов, включая самые большие и активные в мире.",
            published_at: "31.01.2024",
            featured_image: 'https://gas-kvas.com/grafic/uploads/posts/2023-10/1696557268_gas-kvas-com-p-kartinki-vulkan-1.jpg',
            url_key: 'vulkans',
        },{
            id: 7,
            name: "Ауси",
            description: "Ауси - это австралийская профессия, связанная с обслуживанием и ремонтом автомобилей. Ауси работают в мастерских, на дорогах и парковках, а также занимаются продажей запчастей и аксессуаров.",
            published_at: "31.01.2024",
            featured_image: 'https://i3.imageban.ru/out/2023/03/07/6215beb9e503f4497ac86247798a6037.jpg',
            url_key: 'ausi'
        },{
            id: 8,
            name: "Аниме",
            description: "Аниме - это японская анимация, которая стала популярной во всем мире. Она имеет яркие персонажи, уникальные миры и разнообразные жанры.",
            published_at: "31.01.2024",
            featured_image: 'https://i1.wp.com/img10.reactor.cc/pics/post/full/Anime-Ero-Oppai-Anime-Ero-Anime-Anime-Ero-Swim-5160420.png?;ssl=1',
            url_key: 'anime'
        },{
            id: 9,
            name: "Природа",
            description: "Природа - это наш дом, который мы должны беречь и охранять. Она дает нам пищу, воду и чистый воздух, а также радует нас своей красотой и разнообразием.",
            published_at: "31.01.2024",
            featured_image: 'https://image.winudf.com/v2/image1/Y29tLk1ZdGVjaC5IRF80S19XYWxscGFwZXJzXzIwMTlfc2NyZWVuXzJfMTU1MDEwMzA4NF8wNDc/screen-2.jpg?fakeurl=1&type=.jpg',
            url_key: 'nature',

        },{
            id: 10,
            name: "Автомобили",
            description: "Автомобили - это средства передвижения, которые мы используем каждый день. Они бывают разных типов и марок, и каждый имеет свои особенности и преимущества.",
            published_at: "31.01.2024",
            featured_image: 'https://sportishka.com/uploads/posts/2021-12/1639454381_1-sportishka-com-p-krutie-retro-avtomobili-sport-krasivo-foto-1.jpg',
            url_key: 'cars',

        },{
            id: 11,
            name: "Море",
            description: "Море - это огромное пространство соленой воды, которое покрывает большую часть нашей планеты. Оно является домом для множества живых существ и источником ресурсов для человека.",
            published_at: "31.01.2024",
            featured_image: 'https://grandtur.files.wordpress.com/2017/06/11.jpg',
            url_key: 'sea',

        },{
            id: 12,
            name: "Говнище",
            description: "Коммунальные Службы обеспечивают комфорт и безопасность жителей, отвечая за водоснабжение, канализацию, отопление и другие услуги.",
            published_at: "31.01.2024",
            featured_image: 'https://nsk.bfm.ru/storage/article/April2022/y0tSz1GVLWbtVqSPVJnESzabqgPByW6T8dWjE5I9.jpg',
            url_key: 'shit'
        }]))
    } catch (e) {
        console.log(e)
    }
}

function* getLatestPosts() {
    try{
        // @ts-ignore
        const state = yield select();
        const posts = state.posts.posts;

        // @ts-ignore
        const latestPosts = yield posts.slice(posts.length - 4, posts.length);
        yield put(setLatestPostsAction(latestPosts));
    } catch (e) {
        console.log(e);
    }
}

function* getCurrentPost() {
    try{
        const path = window.location.pathname.split('/');
        // @ts-ignore
        const state = yield select();
        // @ts-ignore
        const posts = yield state.posts.posts;

        // @ts-ignore
        const post = yield posts.filter((post: IPost) => post.url_key === path[path.length - 1]);

        if (post.length) {
            yield put(setCurrentPostAction(post[0]));
        }
    } catch (e) {
        console.log(e);
    }
}


function* postsEffectAll() {
    yield takeEvery(POSTS_TYPES.GET_POSTS, getPosts);
    yield takeEvery(POSTS_TYPES.GET_LATEST_POSTS, getLatestPosts);
    yield takeEvery(POSTS_TYPES.GET_CURRENT_POST, getCurrentPost);
}

export default postsEffectAll;
