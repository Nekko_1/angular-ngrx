import {POSTS_TYPES, PostState, PostsAction, initialState} from "./post";

export const postReducer = (state: PostState = initialState, action: PostsAction): PostState =>{
    switch (action.type) {
        case POSTS_TYPES.SET_MANY_POSTS:
            return {
                ...state,
                posts: action.posts
            };
        case POSTS_TYPES.SET_LATEST_POSTS:
            return {
                ...state,
                latest_posts: action.posts
            };
        case POSTS_TYPES.SET_CURRENT_POST:
            return {
                ...state,
                current_post: action.post
            };
        default:
            return state;
    }
}