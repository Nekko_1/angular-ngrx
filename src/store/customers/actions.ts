import {CUSTOMER_TYPES, ICustomer} from "./customer";

export const getCustomerByIdAction = (id: number) => ({
	type: CUSTOMER_TYPES.GET_CUSTOMER_BY_ID,
	id
});

export const setCurrentCustomerAction = (customer: ICustomer) => ({
	type: CUSTOMER_TYPES.SET_CURRENT_CUSTOMER,
	customer
});

export const authorizationAction = () => ({
	type: CUSTOMER_TYPES.AUTHORIZATION
});

export const signInAction = (payload: { email: any; password: any; }) => ({
	type: CUSTOMER_TYPES.SIGN_IN_CUSTOMER,
	payload
});

export const signOutAction = (payload: { name: any; email: any; password: any; }) => ({
	type: CUSTOMER_TYPES.SIGN_OUT_CUSTOMER,
	payload
});

export const logoutAction = () => ({
	type: CUSTOMER_TYPES.LOGOUT_CUSTOMER
});

export const getCustomersAction = () => ({
	type: CUSTOMER_TYPES.GET_CUSTOMERS
});

export const setManyCustomersAction = (customers: ICustomer[]) => ({
	type: CUSTOMER_TYPES.SET_MANY_CUSTOMERS,
	customers
});

export const deleteCustomerByIdAction = (id: number) => ({
	type: CUSTOMER_TYPES.DELETE_CUSTOMER_BY_ID,
	id
});
