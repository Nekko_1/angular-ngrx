import {put, takeEvery} from "redux-saga/effects";
import {CUSTOMER_TYPES} from "./customer";
import {setCurrentCustomerAction, setManyCustomersAction} from "./actions";

function* authorization() {
	try {
		// const response = yield call(fetch, "https://jsonplaceholder.typicode.com/users/" + action.id);
		// const user = yield response.json();

		yield put(setCurrentCustomerAction({
			id: 1,
			email: "stopenjoy64@gmail.com",
			name: "sergey"
		}));
	} catch (e) {
		console.log(e);
	}
}

// function* getCustomerById(action: { id: number; }) {
// 	try {
// 		// const response = yield call(fetch, "https://jsonplaceholder.typicode.com/users/" + action.id);
// 		// const user = yield response.json();
//
// 		yield put(setCurrentCustomerAction({
// 			id: 1,
// 			email: "stopenjoy64@gmail.com",
// 			name: "sergey"
// 		}));
// 	} catch (e) {
// 		console.log(e);
// 	}
// }

function* getCustomers() {
	try {
		// const response = yield call(fetch, "https://jsonplaceholder.typicode.com/users");
		// const users = yield response.json();

		yield put(setManyCustomersAction([{
			id: 1,
			email: "stopenjoy64@gmail.com",
			name: "sergey"
		},{
			id: 2,
			email: "sasha@gmail.com",
			name: "sasha"
		}]));
	} catch (e) {
		console.log(e);
	}
}

function* logoutCustomer() {
	try {
		localStorage.removeItem('token');
		yield put(setCurrentCustomerAction({
			id: 0,
			email: "",
			name: ""
		}));
	} catch (e) {
		console.log(e);
	}
}

function* signInCustomer(action: any) {
	try {
		localStorage.setItem('token', 'token');

		yield put(setCurrentCustomerAction({
			id: 1,
			email: "stopenjoy64@gmail.com",
			name: "sergey"
		}));
	} catch (e) {
		console.log(e);
	}
}

function* signOutCustomer(action: any) {
	try {
		// const response = yield call(fetch, "https://jsonplaceholder.typicode.com/users/" + action.id);
		// const user = yield response.json();

		localStorage.setItem('token', 'token');
		yield put(setCurrentCustomerAction({
			id: 1,
			email: "stopenjoy64@gmail.com",
			name: "sergey"
		}));
	} catch (e) {
		console.log(e);
	}
}

function* customersEffectAll() {
	yield takeEvery(CUSTOMER_TYPES.GET_CUSTOMERS, getCustomers);
	// yield takeEvery(CUSTOMER_TYPES.GET_CUSTOMER_BY_ID, getCustomerById);
	yield takeEvery(CUSTOMER_TYPES.AUTHORIZATION, authorization);
	yield takeEvery(CUSTOMER_TYPES.LOGOUT_CUSTOMER, logoutCustomer);
	yield takeEvery(CUSTOMER_TYPES.SIGN_IN_CUSTOMER, signInCustomer);
	yield takeEvery(CUSTOMER_TYPES.SIGN_OUT_CUSTOMER, signOutCustomer);
}

export default customersEffectAll;
