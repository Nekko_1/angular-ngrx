 import {CUSTOMER_TYPES, CustomerState, CustomerAction, initialState} from "./customer";

export const customerReducer = (state = initialState, action: CustomerAction): CustomerState => {
	switch (action.type) {
		case CUSTOMER_TYPES.SET_CURRENT_CUSTOMER:
			return {
				...state,
				current_customer: action.customer
			};
		case CUSTOMER_TYPES.SET_MANY_CUSTOMERS:
			return {
				...state,
				customers: [...state.customers, ...action.customers]
			};
		case CUSTOMER_TYPES.DELETE_CUSTOMER_BY_ID:
			return {
				...state,
				customers: state.customers.filter(customer => customer.id !== action.id)
			};
		default:
			return state;
	}
};
