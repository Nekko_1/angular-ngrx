export enum CUSTOMER_TYPES {
    GET_CUSTOMER_BY_ID = "GET_CUSTOMER_BY_ID",
    SET_CURRENT_CUSTOMER = "SET_CURRENT_CUSTOMER",
    AUTHORIZATION = "AUTHORIZATION",
    LOGOUT_CUSTOMER = "LOGOUT_CUSTOMER",
    SIGN_IN_CUSTOMER = "SIGN_IN_CUSTOMER",
    SIGN_OUT_CUSTOMER = "SIGN_OUT_CUSTOMER",
    GET_CUSTOMERS = "GET_CUSTOMERS",
    SET_MANY_CUSTOMERS = "SET_MANY_CUSTOMERS",
    DELETE_CUSTOMER_BY_ID = "DELETE_CUSTOMER_BY_ID"
}

export interface ICustomer {
    id: number,
    email: string,
    name: string
}

export interface CustomerState {
    customers: ICustomer[],
    current_customer: ICustomer
}

export const initialState: CustomerState = {
    customers: [],
    current_customer: {
        id: 0,
        email: "",
        name: ""
    }
}

interface SetCurrentCustomerAction {
    type: CUSTOMER_TYPES.SET_CURRENT_CUSTOMER;
    customer: ICustomer
}

interface SetManyCustomersAction {
    type: CUSTOMER_TYPES.SET_MANY_CUSTOMERS;
    customers: ICustomer[]
}

interface DeleteCustomerByIdAction {
    type: CUSTOMER_TYPES.DELETE_CUSTOMER_BY_ID;
    id: number
}

export type CustomerAction = SetCurrentCustomerAction | SetManyCustomersAction | DeleteCustomerByIdAction
