import {put, takeEvery} from "redux-saga/effects";
import {setManyProductsAction} from "./actions";
import {PRODUCTS_TYPES} from "./product";
import productsFixture from "./fixture";

function* getProducts() {
    try {
        yield  put(setManyProductsAction (productsFixture));
    } catch (e) {
        console.log(e);
    }
}

function* getProductsByCategoryId () {
    try {
        yield  put(setManyProductsAction (productsFixture));
    } catch (e) {
        console.log(e);
    }
}

function* productsEffectAll () {
    yield takeEvery(PRODUCTS_TYPES.GET_PRODUCTS, getProducts);
    yield takeEvery(PRODUCTS_TYPES.GET_PRODUCTS_BY_CATEGORY_ID, getProductsByCategoryId);
}

export default productsEffectAll;
