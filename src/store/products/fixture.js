const productsFixture = [{
  "id": 1,
  "title": "Product 1",
  "description": "Description",
  "price": 100,
  "special_price": 50,
  "url_key": "",
  "sku": "",
  "status": true,
  "image": "catalog/default.png",
  "category_id": 1,
  "rating": 1,
  "reviewCount": 5
},{
  "id": 2,
  "title": "Product 2",
  "description": "Description",
  "price": 100,
  "special_price": 0,
  "url_key": "",
  "sku": "",
  "status": true,
  "image": "catalog/default.png",
  "category_id": 1,
  "rating": 2,
  "reviewCount": 10
},{
  "id": 3,
  "title": "Product 3",
  "description": "Description",
  "price": 200,
  "special_price": 0,
  "url_key": "",
  "sku": "",
  "status": true,
  "image": "catalog/default.png",
  "category_id": 1
},{
  "id": 4,
  "title": "Product 4",
  "description": "Description",
  "price": 200,
  "special_price": 0,
  "url_key": "",
  "sku": "",
  "status": true,
  "image": "catalog/default.png",
  "category_id": 1,
  "rating": 5,
  "reviewCount": 100
}];

export default productsFixture;
