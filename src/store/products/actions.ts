import {PRODUCTS_TYPES, IProduct} from "./product";

export const getProductsAction = () => ({
    type: PRODUCTS_TYPES.GET_PRODUCTS
});

export const getProductsByCategoryIdAction = (id: number) => ({
    type: PRODUCTS_TYPES.GET_PRODUCTS_BY_CATEGORY_ID,
    id
});

export const setManyProductsAction = (products: IProduct[]) => ({
    type: PRODUCTS_TYPES.SET_MANY_PRODUCTS,
    products
});
