import {PRODUCTS_TYPES, ProductState, ProductsAction, initialState} from "./product";

export const productReducer = (state = initialState, action: ProductsAction): ProductState => {
    switch (action.type) {
        case PRODUCTS_TYPES.SET_MANY_PRODUCTS:
            return {
                ...state,
                products: action.products
            };
        default:
            return state;
    }
}