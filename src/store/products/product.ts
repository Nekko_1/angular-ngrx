export enum PRODUCTS_TYPES {
    SET_MANY_PRODUCTS = "SET_MANY_PRODUCTS",
    GET_PRODUCTS = "GET_PRODUCTS",
    GET_PRODUCTS_BY_CATEGORY_ID = "GET_PRODUCTS_BY_CATEGORY_ID"
}

export interface IProduct {
    id: number,
    title: string,
    description: string,
    price: number,
    special_price: number,
    url_key: string,
    sku: string,
    status: boolean,
    image: string,
    rating?: number,
    reviewCount?: number
}

export interface ProductState {
    products: IProduct[];
    current_product: IProduct;
}

export const initialState: ProductState = {
    products: [],
    current_product: {
        id: 0,
        title: "",
        description: "",
        price: 0,
        special_price: 0,
        url_key: "",
        sku: "",
        status: true,
        image: ""
    }
}

interface getProductsAction {
    type: PRODUCTS_TYPES.GET_PRODUCTS;
    products: IProduct;
}

interface getProductsByCategoryIdAction {
    type: PRODUCTS_TYPES.GET_PRODUCTS_BY_CATEGORY_ID;
    id: number;
}

interface setManyProductsAction {
    type: PRODUCTS_TYPES.SET_MANY_PRODUCTS;
    products: IProduct[];
}

export type ProductsAction =  getProductsAction | getProductsByCategoryIdAction | setManyProductsAction
