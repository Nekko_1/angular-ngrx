import {CATEGORY_TYPES, CategoryActions, CategoryState, initialState} from "./category";

export const categoryReducer = (state = initialState, action: CategoryActions): CategoryState => {
    switch (action.type) {
        case CATEGORY_TYPES.SET_MANY_CATEGORIES:
            return {
                ...state,
                categories: action.categories
            };
        default:
            return state;
    }
}