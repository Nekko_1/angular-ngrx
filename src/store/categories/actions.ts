import {CATEGORY_TYPES, ICategory} from "./category";

export const getCategoriesAction = () => ({
    type: CATEGORY_TYPES.GET_CATEGORIES
});

export const getCurrentCategoryAction = () => ({
    type: CATEGORY_TYPES.GET_CURRENT_CATEGORY
});

export const setCurrentCategoryAction = (category: ICategory) => ({
    type: CATEGORY_TYPES.SET_CURRENT_CATEGORY,
    category
});

export const setManyCategoriesAction = (categories: ICategory[]) => ({
    type: CATEGORY_TYPES.SET_MANY_CATEGORIES,
    categories
});
