export enum CATEGORY_TYPES {
    GET_CATEGORIES = "GET_CATEGORIES",
    SET_MANY_CATEGORIES = "SET_MANY_CATEGORIES",
    GET_CURRENT_CATEGORY = "GET_CURRENT_CATEGORY",
    SET_CURRENT_CATEGORY = "SET_CURRENT_CATEGORY"
}

export interface ICategory {
    id: number,
    parent_id: number,
    title: string,
    description: string,
    image: string,
    url_key: string,
    status: string,
    items?: ICategory[]
}

export interface CategoryState {
    categories: ICategory[],
    current_category: ICategory
}

export const initialState: CategoryState = {
    categories: [],
    current_category: {
        id: 0,
        parent_id: 0,
        title: "",
        description: "",
        url_key: "",
        status: "",
        image: ""
    }
}

interface getCategoriesAction {
    type: CATEGORY_TYPES.GET_CATEGORIES
    categories: ICategory
}

interface setManyCategoryActions {
    type: CATEGORY_TYPES.SET_MANY_CATEGORIES
    categories: ICategory[]
}

interface setCurrentCategoryAction {
    type: CATEGORY_TYPES.SET_CURRENT_CATEGORY
    categories: ICategory
}

export type CategoryActions = setManyCategoryActions | getCategoriesAction | setCurrentCategoryAction
