import {put, takeEvery} from "redux-saga/effects";
import {CATEGORY_TYPES} from "./category";
import {setManyCategoriesAction} from "./actions";
import categoriesFixture from "./fixture";

function* getCategories() {
    try {
        yield put(setManyCategoriesAction(categoriesFixture))
    } catch (e) {
        console.log(e)
    }
}

function* categoriesEffectAll () {
    yield takeEvery(CATEGORY_TYPES.GET_CATEGORIES, getCategories);
}

export default categoriesEffectAll;
