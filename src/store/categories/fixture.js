const categoriesFixture = [{
  "id": 1,
  "parent_id": 0,
  "title": "Общий каталог",
  "description": "",
  "url_key": "",
  "status": "",
  "image": "",
  "items": [{
    "id": 9,
    "parent_id": 1,
    "title": "Mercedes",
    "description": "",
    "url_key": "",
    "status": "",
    "image": "menu/Mercedes.svg"
  },{
    "id": 10,
    "parent_id": 1,
    "title": "Renault",
    "description": "",
    "url_key": "",
    "status": "",
    "image": "menu/Renault.svg"
  },{
    "id": 11,
    "parent_id": 1,
    "title": "SCANIA",
    "description": "",
    "url_key": "",
    "status": "",
    "image": "menu/Scania.svg"
  },{
    "id": 12,
    "parent_id": 1,
    "title": "MAN",
    "description": "",
    "url_key": "",
    "status": "",
    "image": "menu/Man.svg"
  },{
    "id": 13,
    "parent_id": 1,
    "title": "SAF",
    "description": "",
    "url_key": "",
    "status": "",
    "image": "menu/Saf.svg"
  },{
    "id": 14,
    "parent_id": 1,
    "title": "DAF",
    "description": "",
    "url_key": "",
    "status": "",
    "image": "menu/Daf.svg"
  },{
    "id": 15,
    "parent_id": 1,
    "title": "Schmitz",
    "description": "",
    "url_key": "",
    "status": "",
    "image": "menu/Schmitz.svg"
  },{
    "id": 16,
    "parent_id": 1,
    "title": "Krone",
    "description": "",
    "url_key": "",
    "status": "",
    "image": "menu/Krone.svg"
  },{
    "id": 17,
    "parent_id": 1,
    "title": "Kamaz",
    "description": "",
    "url_key": "",
    "status": "",
    "image": "menu/Kamaz.svg"
  },{
    "id": 18,
    "parent_id": 1,
    "title": "ТОНАР",
    "description": "",
    "url_key": "",
    "status": "",
    "image": "menu/Tonar.svg"
  },{
    "id": 19,
    "parent_id": 1,
    "title": "BPW",
    "description": "",
    "url_key": "",
    "status": "",
    "image": "menu/Bpw.svg"
  },{
    "id": 20,
    "parent_id": 1,
    "title": "ROR",
    "description": "",
    "url_key": "",
    "status": "",
    "image": "menu/Ror.svg"
  },{
    "id": 21,
    "parent_id": 1,
    "title": "Volvo",
    "description": "",
    "url_key": "",
    "status": "",
    "image": "menu/Volvo.svg"
  },{
    "id": 22,
    "parent_id": 1,
    "title": "Kassbohrer",
    "description": "",
    "url_key": "",
    "status": "",
    "image": "menu/Kassbohrer.svg"
  },{
    "id": 23,
    "parent_id": 1,
    "title": "Gigant",
    "description": "",
    "url_key": "",
    "status": "",
    "image": "menu/Gigant.svg"
  },{
    "id": 24,
    "parent_id": 1,
    "title": "Grunwald",
    "description": "",
    "url_key": "",
    "status": "",
    "image": "menu/Grunwald.svg"
  }]
},{
  "id": 2,
  "parent_id": 20,
  "title": "Кузов",
  "description": "",
  "url_key": "",
  "status": "",
  "image": "",
  "items": [{
    "id": 25,
    "parent_id": 2,
    "title": "Кузов",
    "description": "",
    "url_key": "",
    "status": "",
    "image": ""
  },{
    "id": 26,
    "parent_id": 2,
    "title": "Окно ветровое",
    "description": "",
    "url_key": "",
    "status": "",
    "image": ""
  },{
    "id": 27,
    "parent_id": 2,
    "title": "Передок кузова",
    "description": "",
    "url_key": "",
    "status": "",
    "image": ""
  },{
    "id": 28,
    "parent_id": 2,
    "title": "Крыша кузова",
    "description": "",
    "url_key": "",
    "status": "",
    "image": ""
  },{
    "id": 29,
    "parent_id": 2,
    "title": "Дверь передняя",
    "description": "",
    "url_key": "",
    "status": "",
    "image": ""
  },{
    "id": 30,
    "parent_id": 2,
    "title": "Вентиляция и отопление",
    "description": "",
    "url_key": "",
    "status": "",
    "image": ""
  },{
    "id": 31,
    "parent_id": 2,
    "title": "Принадлежности",
    "description": "",
    "url_key": "",
    "status": "",
    "image": ""
  },{
    "id": 32,
    "parent_id": 2,
    "title": "Капот, крылья, облицовка радиатора",
    "description": "",
    "url_key": "",
    "status": "",
    "image": ""
  }]
},{
  "id": 3,
  "parent_id": 30,
  "title": "Двигатель",
  "description": "",
  "url_key": "",
  "status": "",
  "image": "",
  "items": [{
    "id": 33,
    "parent_id": 3,
    "title": "Двигатель",
    "description": "",
    "url_key": "",
    "status": "",
    "image": ""
  },{
    "id": 34,
    "parent_id": 3,
    "title": "Система питания",
    "description": "",
    "url_key": "",
    "status": "",
    "image": ""
  },{
    "id": 35,
    "parent_id": 3,
    "title": "Система выпусков отработанных газов",
    "description": "",
    "url_key": "",
    "status": "",
    "image": ""
  },{
    "id": 36,
    "parent_id": 3,
    "title": "Система охлаждения",
    "description": "",
    "url_key": "",
    "status": "",
    "image": ""
  }]
},{
  "id": 4,
  "parent_id": 40,
  "title": "Трансмиссия",
  "description": "",
  "url_key": "",
  "status": "",
  "image": "",
  "items": [{
    "id": 37,
    "parent_id": 4,
    "title": "Сцепление",
    "description": "",
    "url_key": "",
    "status": "",
    "image": ""
  },{
    "id": 38,
    "parent_id": 4,
    "title": "Коробка передач",
    "description": "",
    "url_key": "",
    "status": "",
    "image": ""
  },{
    "id": 39,
    "parent_id": 4,
    "title": "Мост задний",
    "description": "",
    "url_key": "",
    "status": "",
    "image": ""
  }]
},{
  "id": 5,
  "parent_id": 50,
  "title": "Ходовая часть",
  "description": "",
  "url_key": "",
  "status": "",
  "image": "",
  "items": [{
    "id": 40,
    "parent_id": 5,
    "title": "Подвеска",
    "description": "",
    "url_key": "",
    "status": "",
    "image": ""
  },{
    "id": 41,
    "parent_id": 5,
    "title": "Оси передняя и задняя",
    "description": "",
    "url_key": "",
    "status": "",
    "image": ""
  }]
},{
  "id": 6,
  "parent_id": 60,
  "title": "Механизмы управления",
  "description": "",
  "url_key": "",
  "status": "",
  "image": "",
  "items": [{
    "id": 42,
    "parent_id": 6,
    "title": "Управление рулевое",
    "description": "",
    "url_key": "",
    "status": "",
    "image": ""
  },{
    "id": 43,
    "parent_id": 6,
    "title": "Тормоза",
    "description": "",
    "url_key": "",
    "status": "",
    "image": ""
  }]
},{
  "id": 7,
  "parent_id": 70,
  "title": "Электрооборудование",
  "description": "",
  "url_key": "",
  "status": "",
  "image": "",
  "items": [{
    "id": 44,
    "parent_id": 7,
    "title": "Электрооборудование",
    "description": "",
    "url_key": "",
    "status": "",
    "image": ""
  },{
    "id": 45,
    "parent_id": 7,
    "title": "Приборы и датчики",
    "description": "",
    "url_key": "",
    "status": "",
    "image": ""
  }]
},{
  "id": 8,
  "parent_id": 80,
  "title": "Масла и автохимия",
  "description": "",
  "url_key": "",
  "status": "",
  "image": "",
  "items": [{
    "id": 46,
    "parent_id": 8,
    "title": "Автокосметика",
    "description": "",
    "url_key": "",
    "status": "",
    "image": ""
  },{
    "id": 47,
    "parent_id": 8,
    "title": "Антисептики",
    "description": "",
    "url_key": "",
    "status": "",
    "image": ""
  },{
    "id": 48,
    "parent_id": 8,
    "title": "Герметики",
    "description": "",
    "url_key": "",
    "status": "",
    "image": ""
  },{
    "id": 49,
    "parent_id": 8,
    "title": "Жидкости для омывания стекла",
    "description": "",
    "url_key": "",
    "status": "",
    "image": ""
  },{
    "id": 50,
    "parent_id": 8,
    "title": "Жидкости тормозные",
    "description": "",
    "url_key": "",
    "status": "",
    "image": ""
  },{
    "id": 51,
    "parent_id": 8,
    "title": "Масла компрессора кондиционера",
    "description": "",
    "url_key": "",
    "status": "",
    "image": ""
  },{
    "id": 52,
    "parent_id": 8,
    "title": "Масла прочие",
    "description": "",
    "url_key": "",
    "status": "",
    "image": ""
  },{
    "id": 53,
    "parent_id": 8,
    "title": "Смазки",
    "description": "",
    "url_key": "",
    "status": "",
    "image": ""
  },{
    "id": 54,
    "parent_id": 8,
    "title": "Автохимия",
    "description": "",
    "url_key": "",
    "status": "",
    "image": ""
  },{
    "id": 55,
    "parent_id": 8,
    "title": "Ароматизаторы",
    "description": "",
    "url_key": "",
    "status": "",
    "image": ""
  },{
    "id": 56,
    "parent_id": 8,
    "title": "Жидкости гидравлические",
    "description": "",
    "url_key": "",
    "status": "",
    "image": ""
  },{
    "id": 57,
    "parent_id": 8,
    "title": "Жидкости охлаждающие",
    "description": "",
    "url_key": "",
    "status": "",
    "image": ""
  },{
    "id": 58,
    "parent_id": 8,
    "title": "Клей",
    "description": "",
    "url_key": "",
    "status": "",
    "image": ""
  },{
    "id": 59,
    "parent_id": 8,
    "title": "Масла моторные",
    "description": "",
    "url_key": "",
    "status": "",
    "image": ""
  },{
    "id": 60,
    "parent_id": 8,
    "title": "Масла трансмиссионные",
    "description": "",
    "url_key": "",
    "status": "",
    "image": ""
  }]
}];

export default categoriesFixture;
