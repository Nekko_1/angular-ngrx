import {legacy_createStore as createStore, combineReducers, applyMiddleware} from "redux";
import {customerReducer} from "./customers/reducer";
import {composeWithDevTools} from "redux-devtools-extension";
import createSagaMiddleware from "redux-saga";
import {categoryReducer} from "./categories/reducer";
import categoriesEffectAll from "./categories/effects";
import customersEffectAll from "./customers/effects";
import {productReducer} from "./products/reducer";
import productsEffectAll from "./products/effects";
import {postReducer} from "./posts/reducer";
import postsEffectAll from "./posts/effects";
import {CustomerState} from "./customers/customer";
import {ProductState} from "./products/product";
import {CategoryState} from "./categories/category";
import {PostState} from "./posts/post";

const sagaMiddleware = createSagaMiddleware();

export interface IRootState {
	customers: CustomerState;
	products: ProductState;
	categories: CategoryState;
	posts: PostState;
}

const rootReducer = combineReducers({
	customers: customerReducer,
	products: productReducer,
	categories: categoryReducer,
	posts: postReducer
});

export const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(sagaMiddleware)));

sagaMiddleware.run(customersEffectAll);
sagaMiddleware.run(categoriesEffectAll);
sagaMiddleware.run(productsEffectAll);
sagaMiddleware.run(postsEffectAll);
