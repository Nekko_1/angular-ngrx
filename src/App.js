import './App.css';
import {Routes, Route, BrowserRouter} from "react-router-dom";

import Header from "./components/header/Header";
import Footer from "./components/footer/Footer";
import {HomePage} from "./pages/HomePage";
import {NotFoundPage} from "./pages/NotFoundPage";
import {CustomerPage} from "./pages/CustomerPage";
import {DashboardPage} from "./pages/admin/DashboardPage";
import {ResetPage} from "./pages/ResetPage";
import {CategoryPage} from "./pages/CategoryPage";
import {ProductPage} from "./pages/ProductPage";
import {CustomersPage} from "./pages/admin/CustomersPage";
import {NewsPage} from "./pages/NewsPage";
import {SingleNewsPage} from "./pages/SingleNewsPage";
import {PostsPage} from "./pages/admin/PostsPage";

const App = () => {
    return (
        <div className="wrapper">
            <BrowserRouter>
                <Header />

                <Routes>
                    {/* Frontend */}
                    <Route path="/" element={<HomePage />} />

                    <Route path="/news" element={<NewsPage />} />
                    <Route path="/news/:slug" element={<SingleNewsPage />}/>

                    <Route path="/reset" element={<ResetPage />} />
                    <Route path="/customer" element={<CustomerPage />} />

                    <Route path="/category/:slug" element={<CategoryPage />}/>

                    <Route path="/product/:slug" element={<ProductPage />}/>

                    {/* Admin */}
                    <Route path="/admin/dashboard" element={<DashboardPage />}/>
                    <Route path="/admin/customers" element={<CustomersPage />}/>
                    <Route path="/admin/categories" element={<CategoryPage />}/>
                    <Route path="/admin/products" element={<ProductPage />}/>
                    <Route path="/admin/posts" element={<PostsPage />}/>

                    {/* Global */}
                    <Route path="/404" element={<NotFoundPage />} />
                    <Route path="*" element={<NotFoundPage />} />
                </Routes>

                <Footer />
            </BrowserRouter>
        </div>
    );
}

export default App;
