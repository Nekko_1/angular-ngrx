import React from "react";
import {StarIcon} from "@heroicons/react/16/solid";
import Image from "../core/Image";

const ProductItemGrid = ({product})  => {
    return (
        <div key={'grid-' + product.id} className="group relative border-b border-r border-gray-200 p-4 sm:p-6">
            <div className="aspect-h-1 aspect-w-1 overflow-hidden rounded-lg group-hover:opacity-75">
                <Image
                    name={product.image}
                    alt={product.title}
                />
            </div>
            <div className="pb-4 pt-10 text-center">
                <h3 className="text-sm font-medium text-gray-900">
                    <a href={product.url_key}>
                        <span aria-hidden="true" className="absolute inset-0" />
                        {product.title}
                    </a>
                </h3>
                { product.rating && (
                    <div className="mt-3 flex flex-col items-center">
                        <p className="sr-only">{product.rating} out of 5 stars</p>
                        <div className="flex items-center">
                            {[0, 1, 2, 3, 4].map((rating) => (
                                <StarIcon
                                    key={rating}
                                    className={`h-5 w-5 flex-shrink-0 ${product.rating && product.rating > rating ? 'text-yellow-400' : 'text-gray-200'}`}
                                    aria-hidden="true"
                                />
                            ))}
                        </div>
                        <p className="mt-1 text-sm text-gray-500">{product?.reviewCount} reviews</p>
                    </div>
                )}
                <p className="mt-4 text-base font-medium text-gray-900">{product.price}</p>
            </div>
        </div>
    );
};

export default ProductItemGrid;
