import React from "react";
import Image from "../core/Image";

const ProductItemList = ({product})  => {
    return (
        <div key={'list-' + product.id} className="flex justify-between gap-x-6 py-5">
            <div className="flex min-w-0 gap-x-4">
                <div className="h-12 w-12 flex-none rounded-full bg-gray-50">
                    <Image name={product.image} alt={product.title} />
                </div>
                <div className="min-w-0 flex-auto">
                    <p className="text-sm font-semibold leading-6 text-gray-900">{product.title}</p>
                    <p className="mt-1 truncate text-xs leading-5 text-gray-500">{product.description}</p>
                </div>
            </div>
            <div className="hidden shrink-0 sm:flex sm:flex-col sm:items-end">
                {product.special_price ? (
                    <>
                        <p className="mt-1 text-xs leading-5 text-gray-500">
                            {product.special_price}
                        </p>
                        <p className="mt-1 text-xs leading-5 text-gray-500">
                            {product.price}
                        </p>
                    </>
                ) : (
                    <p className="mt-1 text-xs leading-5 text-gray-500">
                        {product.price}
                    </p>
                )}
                <p className="text-sm leading-6 text-gray-900">buy</p>
            </div>
        </div>
    );
};

export default ProductItemList;
