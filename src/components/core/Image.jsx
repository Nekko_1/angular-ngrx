import React from "react";
import LazyLoad from "react-lazyload";

// @ts-ignore
const Image = ({ name, alt = "" })  => {
    try {
        // Import image on demand
        const image = require(`../../assets/images/${name}`);

        // If the image doesn't exist. return null
        if (!image) return null;
        return (
            <LazyLoad height={200}>
                <img src={image} alt={alt}/>
            </LazyLoad>
        );
    } catch (error) {
        console.log(`Image with name "${name}" does not exist`);
        return null;
    }
};

export default Image;