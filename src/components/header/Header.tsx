import React, { useState } from 'react';

import './Header.css';
import logoUrl from '../../assets/images/logo.svg';
import {useDispatch, useSelector} from "react-redux";
import {signInAction, signOutAction} from "../../store/customers/actions";
import {Link} from "react-router-dom";
import {setCurrentCategoryAction} from "../../store/categories/actions";
import {IRootState} from "../../store";
import Hamburger from "./Hamburger";
import Image from "../core/Image";

const Header = () => {
	const dispatch = useDispatch();
	const currentCustomer = useSelector((state: IRootState) => state.customers.current_customer);
	// const categories = useSelector((state: IRootState) => state.categories.categories);

	const [signInPopup, setSignInPopup] = useState(false);
	const [signOutPopup, setSignOutPopup] = useState(false);

	const signIn = (event: any) => {
		event.preventDefault();

		const email = event.target.elements.email.value;
		const password = event.target.elements.password.value;

		dispatch(signInAction({
			email: email,
			password: password
		}));
	};

	const signOut = (event: any) => {
		event.preventDefault();

		const name = event.target.elements.name.value;
		const email = event.target.elements.email.value;
		const password = event.target.elements.password.value;

		dispatch(signOutAction({
			name: name,
			email: email,
			password: password
		}));
	};

	return (
		<header>
			<div className="header-top mx-auto">
				<div className="flex">
					<nav className="flex-auto">
						<ul className="flex">
							<div className={'left-bar'}>
								<li className={'location'} key="menu-home">
									<Image name={'icons/location.svg'} alt={'clock'}/>
									<Link to="/" className="text-white py-2 px-4">Свердловская область</Link>
								</li>
								<li className={'phone'} key="menu-news">
									<Image name={'icons/phone.svg'} alt={'clock'}/>
									<Link to="/" className="text-white py-2 px-4">8 (922) 105-02-02</Link>
								</li>
								<li className={'delivery'} key="menu-customers">
									<Link to="/" className="text-white py-2 px-4">Доставка</Link>
								</li>
								<li className={'warranty'} key="menu-categories">
									<Link to="/" className="text-white py-2 px-4">Возврат и гарантии</Link>
								</li>
								{/*<li key="menu-products">*/}
								{/*	<Link to="/" className="text-white py-2 px-4">Products</Link>*/}
								{/*</li>*/}
								{/*<li key="menu-posts">*/}
								{/*	<Link to="/admin/posts" className="text-white py-2 px-4">Posts</Link>*/}
								{/*</li>*/}
							</div>
							{
								!currentCustomer.id
								? (
								<div className={'right-bar'}>
									<li key="menu-4">
										<span className="text-white py-2 px-4">Мобильный автосервис</span>
									</li>
									<li key="menu-2">
										<span className="text-white py-2 px-4"
											  onClick={() => setSignInPopup(!signInPopup)}>
											Login
										</span>
									</li>
									<li key="menu-3">
										<span className="text-white py-2 px-4"
											  onClick={() => setSignOutPopup(!signOutPopup)}>
											Registration
										</span>
									</li>
								</div>
								)
								: (
									<div className={'right-bar'}>
										<li key="menu-4">
											<span className="text-white py-2 px-4">Мобильный автосервис</span>
										</li>
										<li key="menu-5">
											<Link to="/dashboard" className="text-white py-2 px-4">Hello {currentCustomer.name}</Link>
										</li>
									</div>
								)
							}
						</ul>
					</nav>
				</div>

				{!currentCustomer.id && signInPopup && (
						<div className="popup-login absolute p-5 bg-white dark:bg-gray-900 antialiased">
							<form onSubmit={signIn} className="max-w-sm mx-auto">
								<div className="mb-5">
									<label htmlFor="email" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Email</label>
									<input id="email" type="text" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" />
								</div>
								<div className="mb-5">
									<label htmlFor="password" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Password</label>
									<input id="password" type="password" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" />
								</div>
								<button type="submit" className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Login</button>
							</form>
						</div>
					)
				}
				{!currentCustomer.id && signOutPopup && (
						<div className="absolute p-5 bg-white dark:bg-gray-900 antialiased">
							<form onSubmit={signOut} className="max-w-sm mx-auto">
								<div className="mb-5">
									<label htmlFor="name" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Name</label>
									<input id="name" type="text" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" />
								</div>
								<div className="mb-5">
									<label htmlFor="email" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Email</label>
									<input id="email" type="text" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" />
								</div>
								<div className="mb-5">
									<label htmlFor="password" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Password</label>
									<input id="password" type="password" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" />
								</div>
								<button type="submit" className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Registration</button>
							</form>
						</div>
					)
				}
			</div>
			<div className="header-bottom">
				<div className={"logo"}>
					<Image name={'icons/LogoBlack.svg'} alt={'clock'}/>
				</div>
				<div className="button-menu">
					<Hamburger  />
				<div className={"search-container"}>
					{
						<form className="search" action="" method="get">
							<input name="s" placeholder="Номер детали, VIN. Пример – 451 782" type="search"/>
							<button type="submit">
								<Image name={'icons/Search.svg'} alt={'Search'}/>
							</button>
						</form>
					}
				</div>
				<div className={"status"}>
					<Image name={'icons/clock.svg'} alt={'clock'}/>
					Статус заказа
				</div>
				<div className={"favorites"}>
					<Image name={'icons/favorites.svg'} alt={'favorites'}/>
					Избранное
				</div>
				<div className={"basket"}>
					<Image name={'icons/basket.svg'} alt={'basket'}/>
					Корзина
				</div>

				</div>
			</div>
		</header>
	)
}

export default Header;
