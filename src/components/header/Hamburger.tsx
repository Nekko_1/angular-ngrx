import React, { useState } from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Link} from "react-router-dom";
import {setCurrentCategoryAction} from "../../store/categories/actions";
import { ICategory } from "../../store/categories/category";
import {IRootState} from "../../store";
import Image from "../core/Image";

export interface HamburgerProps {
    /** Initial state of our button */
    isInitiallyOpen?: boolean;
}

const Hamburger = (props: HamburgerProps) => {
    const dispatch = useDispatch();
    const categories = useSelector((state: IRootState) => state.categories.categories);
    const {isInitiallyOpen} = props;
    const [isOpen, setIsOpen] = useState(isInitiallyOpen ?? false);
    const [subCategories, setSubCategories] = useState<ICategory[]>([]);
    const [ActiveElement, setActiveElement] = useState<ICategory>();
    const [ActiveSubElement, setActiveSubElement] = useState<ICategory>();

    const handleClick = () => {
        setIsOpen((prev) => !prev);
    };

    // @ts-ignore
    return (
        <>
            <button
                onClick={handleClick}
                type="button"
                className={`z-10 cursor-pointer`}
            >
                <div className={`burger w-8 h-8 flex justify-around flex-col flex-wrap`}>
                    <div
                        className={`bg-white block w-8 h-[0.35rem] rounded transition-all origin-[1px] ${
                            isOpen ? 'rotate-45' : 'rotate-0'
                        }`}
                    />
                    <div
                        className={`bg-white block w-8 h-[0.35rem] rounded transition-all origin-[1px] ${
                            isOpen ? 'opacity-0 bg-transparent' : 'translate-x-0'
                        }`}
                    />
                    <div
                        className={`bg-white block w-8 h-[0.35rem] rounded transition-all origin-[1px] ${
                            isOpen ? 'rotate-[-45deg]' : 'rotate-0'
                        }`}
                    />
                </div>
                <span className={"btn-label"}>Каталог</span>
            </button>
            <div className={`menu-wrapper flex ${ isOpen ? '' : 'hidden'}`}>
                <nav className={'menu-sidebar '}>
                    <div className={"items"}>
                        {
                            categories.length > 0 && (
                                categories.map((category: ICategory) =>
                                    <div className={`item ${ActiveElement?.id === category.id ? 'active' : ''}`} key={'menu-category-' + category.id}>
                                        <Link to={'/category/' + category.url_key} className={`py-2 px-4`} onMouseEnter={() => {setActiveElement(category); setSubCategories(category.items ?? [])}}>{category.title}</Link>
                                    </div>
                                )
                            )
                        }
                    </div>
                </nav>
                <nav className={'menu-content'}>
                    <div className={ActiveElement?.title === 'Общий каталог' ? 'sub-items-all' : 'sub-items'}>
                        {
                            subCategories.length > 0 && (
                                subCategories.map((category: ICategory) =>
                                    <div className={`sub-item ${ActiveSubElement?.id === category.id ? 'sub-active' : ''}`} key={'menu-sub-category-' + category.id}>
                                        <Link to={'/category/' + category.url_key} className={`link-container py-2 px-4 ${ActiveElement?.title === 'Общий каталог' ? 'grayscale hover:grayscale-0' : ''}`} onMouseEnter={()=> {setActiveSubElement(category)}} onClick={() => dispatch(setCurrentCategoryAction(category))}>
                                            {category.image ? (
                                                <Image name={category.image} alt={category.title}/>
                                            ) : (
                                                <span >{category.title}</span>
                                            )}
                                        </Link>
                                    </div>
                                )
                            )
                        }
                    </div>
                </nav>
            </div>
        </>
    );
}

export default Hamburger;
