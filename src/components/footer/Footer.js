import React from 'react';
import './Footer.css';
import logoUrl from '../../assets/images/logo.svg';

const Footer = () => {
	return (
		<footer>
			<img src={logoUrl} alt="Tesla"/>
		</footer>
	)
}

export default Footer;
