import React from 'react';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { setCurrentPostAction } from '../../store/posts/actions';

import './LatestPosts.css';

const LatestPosts = (props) => {
    const dispatch = useDispatch();

	let posts = props.posts;

    const truncate = (str) => {
        return str.length > 60 ? str.substring(0, 60) + "..." : str;
    };

	return (
		<div className="container my-14 m-auto">
            <h2 className="text-3xl font-bold">Latest Posts</h2>

			<div className="flex flex-row flex-wrap">
				{
					posts?.length > 0 && (
						posts.map(post =>
							<div className="basis-1/4 pr-4 my-10" key={'post-' + post.id}>
								<Link to={'/post/' + post.url_key} className="post-box" onClick={() => dispatch(setCurrentPostAction(post))}>
									<div className="featured-post__background mb-5" style={{ backgroundImage: 'url(' + post.featured_image + ')' }}></div>
									<div className="post-box__text-content">
										<h3 className="font-semibold mb-3">{post.name}</h3>
										<p className="mb-3">{truncate(post.description)}</p>
										<p className="text-zinc-500">{post.published_at}</p>
									</div>
								</Link>
							</div>
						)
					)
				}
			</div>
		</div>
	)
}



export default LatestPosts;
